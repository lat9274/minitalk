/*
** xfunction.c for xfunction in /home/wu_d/epitech/tek1/s2/system_unix/minitalk/src
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Wed Mar 20 22:36:06 2013 mauhoi wu
** Last update Fri Mar 22 19:50:58 2013 mauhoi wu
*/

#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include "include/my.h"

void	fexit(char *str)
{
  my_putstr(str);
  exit(EXIT_FAILURE);
  return ;
}

void	my_xputstr(char *str)
{
  my_putstr("Hello !\nI'm the Server\nMode : ");
  my_putstr(str);
  my_putstr("\nMy PID is : ");
  my_put_nbr(getpid());
  my_putstr("\nPress ^C to exit\n");
  return ;
}

void	xexit(char *str)
{
  my_putstr(str);
  exit(EXIT_SUCCESS);
  return ;
}
