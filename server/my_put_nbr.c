/*
** my_put_nbr.c for my_put_nbr in /home/wu_d//refait/1/Jour_1/exo_7
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Sat Oct 20 14:57:27 2012 mauhoi wu
** Last update Thu Mar 21 20:39:58 2013 mauhoi wu
*/

#include "include/my.h"

void	my_put_nbr(int nb)
{
  int	div;

  if (nb == -2147483648)
    my_putstr("-2147483648");
  else
    {
      div = 1;
      if (nb <= -1)
	{
	  my_putchar(45);
	  nb = nb * -1;
	}
      while ((nb / div) >= 10)
	div = div * 10;
      while (div > 0)
	{
	  my_putchar((nb / div) % 10 + '0');
	  div = div / 10;
	}
    }
  return ;
}
