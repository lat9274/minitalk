/*
** my.h for my in /home/wu_d/s2/system-unix/minitalk/src/client/include
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Wed Mar  6 14:19:57 2013 mauhoi wu
** Last update Thu Mar 21 20:00:21 2013 mauhoi wu
*/

#ifndef MY_H_
# define MY_H_

# define USAGE "usage: ./server [OPS]\nOPS : -b binary mode\n"

int	my_putchar(char c);
int	my_strlen(char *str);
int	my_putstr(char *str);
void	my_put_nbr(int nb);
void	fexit(char *str);
void	my_xputstr(char *str);
void	xexit(char *str);

#endif /* !MY_H_ */
