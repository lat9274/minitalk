/*
** main.c for main in /home/wu_d/s2/system-unix/minitalk/src/server
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Wed Mar  6 14:13:56 2013 mauhoi wu
** Last update Fri Mar 22 20:07:30 2013 mauhoi wu
*/

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include "include/my.h"

void		bin_to_str(int signum, char *c, int *bin)
{
  (*bin)--;
  if (signum == SIGUSR1)
    {
      (*c)++;
      if ((*bin) > 0)
	(*c) <<= 1;
    }
  else if (signum == SIGUSR2 && (*bin) > 0)
    (*c) <<= 1;
  return ;
}

void		signal_str(int signum)
{
  static char	c = '\0';
  static int	bin = 8;

  if (signum)
    bin_to_str(signum, &c, &bin);
  if (!bin || !signum)
    {
      my_putchar(c);
      bin = 8;
      c = '\0';
    }
  return ;
}

void		signal_bin(int signum)
{
  if (signum)
    {
      if (signum == SIGUSR1)
	my_putchar('1');
      else if (signum == SIGUSR2)
	my_putchar('0');
    }
  return ;
}

int		main(int ac, char **av)
{
  if (ac == 1)
    {
      my_xputstr("Normal");
      while (42)
	{
	  signal(SIGUSR1, signal_str);
	  signal(SIGUSR2, signal_str);
	  if (!sleep(2))
	    signal_str(0);
	}
    }
  else if (ac == 2 && av[1][0] == '-' && av[1][1] == 'b' && !av[1][2])
    {
      my_xputstr("Binary");
      while (42)
	{
	  signal(SIGUSR1, signal_bin);
	  signal(SIGUSR2, signal_bin);
	  if (!sleep(2))
	    signal_bin(0);
	}
    }
  else
    fexit(USAGE);
  return (EXIT_SUCCESS);
}
