/*
** main.c for main in /home/wu_d/s2/system-unix/minitalk/src/client
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Wed Mar  6 14:13:56 2013 mauhoi wu
** Last update Fri Mar 22 19:42:49 2013 mauhoi wu
*/

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include "include/my.h"

void		c_talk(int pid, char *s, int speed)
{
  unsigned int	i;
  unsigned char	dec;

  i = 0;
  while (s[i])
    {
      dec = 128;
      while (dec > 0)
        {
          if (s[i] & dec)
            xkill(pid, SIGUSR1);
          else
            xkill(pid, SIGUSR2);
          dec >>= 1;
          usleep(speed);
        }
      i++;
    }
  return ;
}

int	main(int ac, char **av)
{
  int	speed;

  speed = 0;
  if (ac == 3)
    c_talk(my_getnbr(av[1]), av[2], 50);
  else if (ac == 5 && av[3][0] == '-' && av[3][1] == 's' && !av[3][2])
    {
      if ((speed = my_getnbr(av[4])) > 0)
	c_talk(my_getnbr(av[1]), av[2], speed);
      else
	my_putstr(MY_WARNING);
    }
  else
    my_putstr(USAGE);
  return (EXIT_SUCCESS);
}
