/*
** my.h for my in /home/wu_d/s2/system-unix/minitalk/src/client/include
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Wed Mar  6 14:19:57 2013 mauhoi wu
** Last update Thu Mar 21 20:52:08 2013 mauhoi wu
*/

#ifndef MY_H_
# define MY_H_

# define USAGE "usage: ./client SERVER_PID STRING_TO_SEND [-s SPEED(us)]\n"
# define MY_WARNING "warning: SPEED must > 0\n"

int	my_putchar(char c);
int	my_strlen(char *str);
int	my_putstr(char *str);
int	my_getnbr(char *str);
void	xkill(int pid, int sig);

#endif /* !MY_H_ */
