/*
** my_getnbr.c for my_getnbr in /home/wu_d//local/colle_02
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Mon Oct 22 09:52:02 2012 mauhoi wu
** Last update Thu Mar 21 20:39:45 2013 mauhoi wu
*/

int	my_getnbr(char *str)
{
  int	i;
  int	neg;
  int	nbr;

  i = 0;
  neg = 1;
  while (str[i] != '\0' && (str[i] == '+' || str[i] == '-'))
    {
      if (str[i] == '-')
	neg *= -1;
      i++;
    }
  str += i;
  i = 0;
  nbr = 0;
  while (str[i] != '\0' && (str[i] >= '0' && str[i] <= '9'))
    {
      nbr *= 10;
      nbr += (str[i] - '0');
      i++;
    }
  return (nbr * neg);
}
