/*
** xfunction.c for xfunction in /home/wu_d/epitech/tek1/s2/system_unix/minitalk/src/client
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Wed Mar 20 21:46:55 2013 mauhoi wu
** Last update Thu Mar 21 20:53:25 2013 mauhoi wu
*/

#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include "include/my.h"

void	xkill(int pid, int sig)
{
  if (kill(pid, sig) == -1)
    {
      my_putstr("Send error\nThe server is opened?\n");
      exit(EXIT_FAILURE);
    }
  return ;
}
