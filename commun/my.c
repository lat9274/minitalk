/*
** my.c for my in /home/wu_d/s2/system-unix/minitalk/src/client
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Wed Mar  6 14:14:28 2013 mauhoi wu
** Last update Thu Mar 21 19:47:03 2013 mauhoi wu
*/

#include <unistd.h>

int	my_putchar(char c)
{
  return (write(1, &c, 1));
}

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    i++;
  return (i);
}

int	my_putstr(char *str)
{
  return (write(1, str, my_strlen(str)));
}
