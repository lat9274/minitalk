##
## Makefile for minitalk in /home/wu_d/s2/system-unix/minitalk
## 
## Made by mauhoi wu
## Login   <wu_d@epitech.net>
## 
## Started on  Wed Mar  6 14:01:55 2013 mauhoi wu
## Last update Thu Mar 21 21:00:10 2013 mauhoi wu
##

NAME	= minitalk

SERVER	= server

CLIENT	= client

COMMUN	= commun

SRC_S	= \
	$(SERVER)/main.c \
	$(SERVER)/my_put_nbr.c \
	$(SERVER)/xfunction.c

SRC_C	= \
	$(CLIENT)/main.c \
	$(CLIENT)/my_getnbr.c \
	$(CLIENT)/xfunction.c

CSRC	= \
	$(COMMUN)/my.c

OBJ_S	= $(SRC_S:.c=.o)

OBJ_C	= $(SRC_C:.c=.o)

COBJ	= $(CSRC:.c=.o)

CC	= cc

CFLAGS	+= -Wall -Wextra -ansi -pedantic
CFLAGS	+= -O2
CFLAGS 	+= -D_GNU_SOURCE

RM	= rm -vf

all: $(NAME)

$(NAME): $(SERVER)/$(SERVER) $(CLIENT)/$(CLIENT)

$(SERVER)/$(SERVER): $(COBJ) $(OBJ_S)
	$(CC) -o $(SERVER)/$(SERVER) $(COBJ) $(OBJ_S)

$(CLIENT)/$(CLIENT): $(COBJ) $(OBJ_C)
	$(CC) -o $(CLIENT)/$(CLIENT) $(COBJ) $(OBJ_C)

clean:
	$(RM) $(OBJ_S) $(OBJ_C) $(COBJ)

fclean: clean
	$(RM) $(SERVER)/$(SERVER) $(CLIENT)/$(CLIENT)

re: fclean all

.PHONY: all re fclean clean minitalk
